---
title: "Quick start"
date: "`r Sys.Date()`"
vignette: >
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
  %\VignetteIndexEntry{Tutorial: Quick start}
output:
  rmarkdown::html_vignette
---

```{r, include = FALSE}
knitr::opts_knit$set(verbose = TRUE)
knitr::opts_chunk$set(fig.align = "center",
                      fig.path = "figures-rmd/z-fig-010-")
options(scipen = 6, width = 85)

library(here)

set.seed(4)
CACHE_FILE <- here("vignettes", "rds-files", "z-cache-tutorial-010-quick-start.rda")
```

This document introduces the basics of `etalon` and how to use it to assess the performance of a statistical model. 

The three usual steps when evaluating a model with `etalon` are:

1. **Simulating data** from known parameter values
2. **Fitting the model** on the simulated data
3. **Comparing** the model output and the known parameter values.

In this tutorial we will get started with a small example. We will examine how taking into account repeated measurements with random effects affects our ability to estimate model parameters.

# Setup

Let's prepare our R session:

```{r message = FALSE, warning = FALSE}
library(etalon)
# We'll use the tidyverse for data wrangling
library(tidyverse)
```

Many `etalon` functions can take advantage of parallel processing using the `future` package. Let's set it up:

```{r message = FALSE, warning = FALSE}
# Setting parallel processing
library(future)
plan(multisession)
```

# A model for fish swimming speed

Let's imagine a project studying the relationship between salmon body length and its top swimming speed. In the lab, we will repeat the swimming speed challenge several times for each salmon and get several speed measurements per fish.

A good statistical model should definitely take into account that we have repeated measurements for each fish. But just how bad would a model be if it assumed that all data was independent?

That is what we are going to explore below! We are going to generate a grid of "true" parameters for the relationship between body length and speed, then simulate some data based on those parameters, and finally run two models on those datasets: one which takes into account the repeated measurements, and one that assumes that all data are independent.

```{r }
z <- sim_table() %>%
  set_params(alpha = seq(20, 40, length.out = 3),
             beta = seq(0.2, 0.6,  length.out = 3),
             sigma_ind = seq(0.5, 2.5, length.out = 5),
             sigma_res = 1) %>%
  replicate_sims(5)

gen_data <- function(alpha, beta, sigma_ind, sigma_res) {
  n_fish <- 10
  n_repl <- 5
  sizes <- runif(n_fish, 20, 80)
  ind_effect <- rnorm(n_fish, 0, sigma_ind)
  fish_id <- rep(1:n_fish, n_repl)
  fish_effect <- ind_effect[fish_id]
  pred_speed <- alpha + beta * sizes[fish_id] + fish_effect
  obs_speed <- rnorm(length(pred_speed), pred_speed, sigma_res)
  return(data.frame(fish = fish_id, size = sizes[fish_id], speed = obs_speed))
}

w <- gen_data(30, 0.5, 2, 1)
plot(w$size, w$speed, col = w$fish)

z <- generate_datasets(z, gen_data)

fit_me <- function(dataset) {
  lme4::lmer(speed ~ size + (1 | fish), data = dataset)
}
fit_lm <- function(dataset) {
  lm(speed ~ size, data = dataset)
}
fit_lm_avg <- function(dataset) {
  dataset <- dataset %>% group_by(fish) %>%
    summarize(speed = mean(speed),
              size = mean(size))
  lm(speed ~ size, data = dataset)
}


w1 <- fit_model(z, fit_lm, method = "lm") %>%
  tidy_estimates(extra_params = c("(Intercept)", "size"),
                 mapping = c("alpha" = "(Intercept)", "beta" = "size")) %>%
  tidy_intervals(extra_params = c("(Intercept)", "size"),
                 conf.level = seq(0.05, 0.95, by = 0.1),
                 mapping = c("alpha" = "(Intercept)", "beta" = "size"))

w2 <- fit_model(z, fit_lm_avg, method = "lm_avg") %>%
  tidy_estimates(extra_params = c("(Intercept)", "size"),
                 mapping = c("alpha" = "(Intercept)", "beta" = "size")) %>%
  tidy_intervals(extra_params = c("(Intercept)", "size"),
                 conf.level = seq(0.05, 0.95, by = 0.1),
                 mapping = c("alpha" = "(Intercept)", "beta" = "size"))

w <- bind_rows(w1, w2)

bias_qplot(w, "beta") + facet_wrap(~ method_fit)

intervals_qplot(w, "beta") + facet_wrap( ~ method_fit)

bias_boxplot(w, "alpha") + facet_wrap( ~ method_fit)


```


# A model for allometry

```{r include = FALSE}
# Rough calculations from Gambusia data (allomdata package)
a <- 24.3
b <- 0.56
# wet mass in g, excretion in ugN/hour
n <- 25
mass <- runif(n, 0.3, 1.8)
log_rate <- log(a) + b * log(mass) + rnorm(n, 0, 0.08)
rate <- exp(log_rate)
plot(mass, rate)
data <- tibble(mass = mass, rate = rate)
```

We want to evaluate a statistical model for the relationship between fish size and nitrogen excretion in [mosquitofish](https://en.wikipedia.org/wiki/Mosquitofish). The data collected from an experiment would measure size and nitrogen excretion rate for a set of fish individuals and look something like this, with one measure per fish:
```{r echo = FALSE}
plot(data, xlab = "Fish mass (g) ", ylab = "Excretion rate (µg/N/h)", las = 1, pch = 19)
head(data)
```

A typical equation used to describe this allometric relationship is:
\begin{align}
    \mathrm{rate} = \alpha \mathrm{mass} ^ \beta
\end{align}
which can also be written:
\begin{align}
    \log(\math{rate}) = \log(\alpha) + \beta \log(\mathrm{mass})
\end{align}

Should we use a linear model based on the log-transformed equation? Or should we use the original equation $\mathrm{rate} = \alpha \mathrm{mass} ^ \beta$ and some optimization method to fit the data directly? Maybe both methods are exactly equivalent? Let's explore this!

# Step 1: simulating data

## Parameter grid

We start by generating a grid a parameter values for $\alpha$ and $\beta$. Reasonable values based on a literature survey are `alpha = 24.3` and `beta = 0.56`. Let's generate a grid around those values.

When using `etalon`, all the simulations are stored in a tidy table, with one simulation per row. We start by initializing a new, empty table to store our simulations:

```{r }
z <- sim_table()
z
```

After initialization `z` is just an empty tibble, but we'll add our parameter grid right now:

```{r }
z <- z %>% set_parameters(alpha = seq(20, 28, by = 1),
                          beta = seq(0.4, 0.8, by = 0.05))
head(z)
```

Now our simulation table has one column, `parameters`, with one row per parameter combination. Each parameter cell contains two values (`alpha` and `beta`), as is shown by `<dbl [2]>`:

```{r }
z[["parameters"]][[1]]
```

There are many ways to generate the parameter grid with `etalon`. Here we used the default behaviour of `set_parameters()` which is to cross all the parameter levels that are passed to it. (WIP: **link to vignette going into details for param grid generation**)

If we want to examine the actual parameter values, we can use `params()` to unpack the `parameters` column:

```{r }
z %>% params()
```

This returns a tibble. A central aspect of the philosophy of `etalon` functions is to try to return **tidy data** whenever possible, so that manipulation and visualization of simulations are made easier. 

At this point, we still have only one row for each combination of values of $\alpha$ and $\beta$. We want more, so that we have several simulated datasets for each parameter combination. We can use the `replicate_sims()` function for that:

```{r }
z <- z %>% replicate_sims(n = 3)
z
```

That's better! Now that we have our parameter grid, we are ready to simulate some data, with one simulated dataset per row of `z`.

## Simulated datasets

To generate simulated datasets, we have to prepare our own data-generating function that we will apply for each row of `z`.

The data-generating function must be consistent with the parameter grid that we prepared above. This means that this function must take as arguments the parameters of our simulation table. As a reminder, our parameters are:
```{r }
params(z)
```
so here our data-generating function must be of the form:
```{r eval = FALSE}
f <- function(alpha, beta) {
  ...
}
```

<div class="alert-primary" role="alert"> 
When we write our data-generating function, we have to think about a mechanism to generate the data from the parameters. Our model is only an approximation of the real world, but having to write a generative function forces us to think explicitly about our assumptions about the data-generating process. It also allows us to notice which other parameters might be needed to define the model, or alternative models.
</div>

In our example, here is a reasonable way that we can come up with to generate data:

```{r }
gen_fun <- function(alpha, beta) {
  # How many individuals? We'll use the same number in all simulations.
  n <- 10
  # What are the wet masses? Let's assume they are uniformly distributed around
  # 0.3 and 1.8 grams.
  mass <- runif(n, 0.3, 1.8)
  # Predicted rates
  log_rate_pred <- log(alpha) + beta * log(mass)
  # But observed rates will have some biological or measurement variation added
  # on top of the predicted rates! Again, we'll use the same amount of
  # variation for all simulations.
  log_rate_obs <- log_rate_pred + rnorm(n, mean = 0, sd = 0.08)
  # Return dataset
  tibble(mass = mass,
         rate = exp(log_rate_obs))
}
```

Notice how in the function above we always use the same sample size (`n = 10`) and the same amount of variation around predicted values (`sd = 0.08`). Of course, we might want to do a more thorough analysis, and `n`and `sd` themselves might be parameters that we would vary along the parameter grid!

<div class="alert-primary" role="alert"> 
Notice also that when we wrote our function we had to think about how to include variation around predicted values. We chose to add variation on the log-transformed scale. Does it make sense? Well, it can be reasonable if we consider mostly biological variation. But what about measurement error? If our measurements of nitrogen concentration in the water itself is noisy, maybe it will look more like some normal noise on the original scale? Maybe we could actually estimate both some biological and some measurement variation in our model! That would be great! Oh, but then we would maybe need replicate measurements per fish? As you can see, writing a generative model forces you to think hard about your experiment!
</div>

For now we'll be happy with this function. Let's test it:

```{r fig.width = 4, fig.height = 4}
plot(gen_fun(alpha = 24, beta = 0.6), las = 1, pch = 19)
```

Looks good! We can use it to generate our many simulations:

```{r }
z <- z %>% generate_datasets(gen_fun)
head(z)
```

Notice how `z` gained a new column `dataset`, which contains one simulated dataset per row:

```{r }
z[["dataset"]][[1]]
```

# Step 2: fitting the model

To generate the simulated datasets, we wrote a function to generate one dataset and we passed it to `generate_datasets()` to apply it to all the rows of our simulation table. In the same way, we have to write a function that fits one model at a time, and then we will use `fit_model()` to apply it to every row in our simulation table.

In this example we will actually write two functions for the fit: 

- one for the fit using a linear model (based on the `lm` function) on log-transformed data
- one for the fit using nonlinear least squares (based on the `nls` function)

A fit function must be of the form:
```{r }
f <- function(dataset) { ... }
```
where `dataset` is one element from the `dataset` column of our simulation table. Let's examine how a `dataset` looks like by looking again at the first row in our table:

```{r }
z$dataset[[1]]
```

This is exactly the output format returned by our `gen_fun()` above (as expected).

Let's write our fit function for the linear model approach:

```{r }
fit_fun_lin <- function(dataset) {
  m <- lm(log(rate) ~ log(mass), data = dataset)
  m
}
```

and the fit function for the nonlinear least squares approach:

```{r }
fit_fun_nls <- function(dataset) {
  m <- nls(rate ~ alpha * mass ^ beta, data = dataset,
           start = list(alpha = 1, beta = 0.5))
  m
}
```

Let's test those two fit functions:

```{r }
fit_fun_lin(z$dataset[[1]])
fit_fun_nls(z$dataset[[1]])
```

Everything looks fine. Now we can apply them to all our simulations:

```{r eval = FALSE}
z_lm <- z %>% fit_model(fit_fun_lin, method = "lm")
z_nls <- z %>% fit_model(fit_fun_nls, method = "nls")
```
```{r include = FALSE}
if (!file.exists(CACHE_FILE)) {
  z_lm <- z %>% fit_model(fit_fun_lin, method = "lm")
  z_nls <- z %>% fit_model(fit_fun_nls, method = "nls")
  save(z_lm, z_nls, file = CACHE_FILE)
} else {
  load(CACHE_FILE)
}
```

# Step 3: tidying the results

In order to compare the performances of both fit methods, we want to extract the parameter estimates from the fits we just performed.

We will do this with the `tidy_estimates()` function. This function will run the `tidy()` function from the `broom` package  on each row of `z` in order to get parameter point estimates.

To better understand how this works, let's just run the `broom::tidy()` function ourselves on some fits:

```{r }
library(broom)
# A lm fit
tidy(z_lm$fit[[1]])
# A nls fit
tidy(z_nls$fit[[1]])
```

Note how the term names in the output for a nls fit match our original parameter names `alpha` and `beta`. However, for the output from a linear fit, the terms are called `(Intercept)` and `log(mass)` by default. We can provide a mapping argument to `tidy_estimates()` to tell it how to translate those to the original parameters: `(Intercept)` is actually log(`alpha`) and `log(mass)` is actually `beta`. Finally, since the original parameter was `alpha` and not log(`alpha`), we must tell `tidy_estimates()` to extract `log(alpha)` as an extra parameter and to transform it with the `exp()` function.

```{r eval = FALSE}
z_lm <- z_lm %>% tidy_estimates(mapping = c("alpha" = "(Intercept)",
                                            "beta" = "log(mass)"),
                                transform = list("alpha" = exp))
z_nls <- z_nls %>% tidy_estimates()
```

In the same way that we extract tidy point estimates with `tidy_estimates()`, we can also extract tidy confidence intervals with `tidy_intervals()`:

```{r eval = FALSE}
z_lm <- z_lm %>% tidy_intervals(mapping = c("alpha" = "(Intercept)",
                                            "beta" = "log(mass)"),
                                transform = list("alpha" = exp),
                                conf.level = seq(0.1, 0.9, by = 0.1))
z_nls <- z_nls %>% tidy_intervals(conf.level = seq(0.1, 0.9, by = 0.1))
```

```{r include = FALSE}
if (!"estimates" %in% colnames(z_lm)) {
  z_lm <- z_lm %>% tidy_estimates(mapping = c("alpha" = "(Intercept)",
                                            "beta" = "log(mass)"),
                                transform = list("alpha" = exp))
  z_nls <- z_nls %>% tidy_estimates()
  z_lm <- z_lm %>% tidy_intervals(mapping = c("alpha" = "(Intercept)",
                                            "beta" = "log(mass)"),
                                transform = list("alpha" = exp),
                                conf.level = seq(0.1, 0.9, by = 0.1))
  z_nls <- z_nls %>% tidy_intervals(conf.level = seq(0.1, 0.9, by = 0.1))
  save(z_lm, z_nls, file = CACHE_FILE)
}
```

Let's put those simulations together in a single table:
```{r }
z <- bind_rows(z_lm, z_nls)
z
```

How does one `estimates` cell look like?
```{r }
z$estimates[[1]]
```
What about one `intervals` cell?
```{r }
z$intervals[[1]]
```

We will focus below on comparing the fit methods for the estimation of `beta`. We could also compare the estimates of `alpha`, but since we would need an extra step to convert `log(alpha)` to `alpha` values for the linear fits, we will ignore this parameter in this Quick Start tutorial.

```{r fig.width = 8, fig.height = 4, message = FALSE, warning = FALSE}
bias_qplot(z, "beta") + facet_wrap(~ method_fit) + xlim(0, 1) + ylim(0, NA)
intervals_qplot(z, "beta") + facet_wrap(~ method_fit)
bias_boxplot(z, "beta") + facet_wrap( ~ method_fit) + xlim(0, 1) + ylim(0, NA)
```

Perfect! Now we have all our simulations, and each row contains a fit object and the corresponding tidy estimates.

# Ideas for other vignettes

Vignette to model the effect of sperm size on EGO, cf. teaching/EKOS314/EKOS314-2019/homework/H6/H6-annotated-script.html#task_b

We could compare two models: (i) where we calculate the percentage of EGO per father and (ii) where we keep all discrete observations, and see if this influences the estimates and uncertainties for the effect of sperm size on EGO.


# Tutorial flow

- Having one Quick Start tutorial that might go in a bit deep, just to showcase the package, and then have step-by-step tutorials that should be followed in order?

# Real-life examples

- isotracer?
- distinguish epi and gen effects in Serratia
- mutation accumulation experiment modelling
- benchmarking ambience

# To do

- hash functions to identify datasets
- add automatic timing?
- graceful handling of parallel runs, and avoiding conflicts with modelling functions running in parallel
- modify tidy_estimates and tidy_intervals so that they automatically extract the parameters given in mapping

