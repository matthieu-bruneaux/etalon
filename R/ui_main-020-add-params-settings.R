### * All functions in this file are exported

### * add_params*()

### ** add_params()

#' Add parameters to a simulation table
#'
#' \code{add_params*()} and \code{add_parameters*()} functions are synonymous.
#' 
#' @param .x A \code{sim_tbl} object. The only columns that it can have when
#'     calling an \code{add_params*()} function are \code{"parameters"} and
#'     \code{"settings"}.
#' @param ... Parameters to add, as named arguments. For \code{add_params()},
#'     each argument is a vector containing all the values to add. For
#'     \code{add_params_seq()}, each argument is a two-value vector specifying
#'     the min and the max values of the sequence to generate. See examples for
#'     more detail.
#' @param .tbl For \code{add_params_tbl}, a data frame or tibble containing the
#'     parameters to add.
#' @param .n For \code{add_params_seq}, the number of values to generate when
#'     building a parameter sequence.
#'
#' @return A \code{sim_tbl} object with the new parameters added to the
#'     \code{"parameters"} column.
#'
#' @family parameter/setting functions
#' 
#' @examples
#' z <- sim_tbl()
#'
#' z <- add_params(z, alpha = rnorm(10), beta = rnorm(10))
#' params(z)
#'
#' z <- add_params_seq(z, gamma = c(-5, 5), .n = 10)
#' params(z)
#'
#' tbl <- tibble::tibble(eta = runif(10), theta = rnorm(10))
#' z <- add_params_tbl(z, tbl)
#' params(z)
#' 
#' @export

add_params <- function(.x, ...) {
    global_check_add_params_settings(.x = .x, ..., from = "add_params")
    new_params <- do.call(cbind, list(...))
    if ("parameters" %in% colnames(.x)) {
        old_params <- params(.x)
        stopifnot(nrow(old_params) == nrow(new_params))
        new_params <- cbind(old_params, new_params)
    }
    p_elements <- list()
    p_names <- colnames(new_params)
    for (i in seq_len(nrow(new_params))) {
        p_elements[[i]] <- unlist(new_params[i, ])
        names(p_elements[[i]]) <- p_names
    }
    p_tbl <- tibble::tibble(parameters = p_elements)
    if (nrow(.x) == 0) {
        .x <- p_tbl
    } else {
        .x[["parameters"]] <- p_tbl[["parameters"]]
    }
    check_post_add_params_settings(.x)
    as_sim_tbl(.x)
}

### ** add_params_seq()

#' @rdname add_params
#' @export

add_params_seq <- function(.x, ..., .n = 2) {
    global_check_add_params_settings(.x = .x, ..., .from = "add_params_seq")
    new_elements <- list(...)
    stopifnot(all(sapply(new_elements, length) == 2))
    new_elements <- lapply(new_elements, function(x) {
        seq(from = x[1], to = x[2], length.out = .n)
    })
    args <- c(list(".x" = .x), new_elements)
    out <- do.call(add_params, args)
    check_post_add_params_settings(out)
    out
}

### ** add_params_tbl()

#' @rdname add_params
#' @export

add_params_tbl <- function(.x, .tbl) {
    tbl <- check_add_params_settings_tbl(.tbl)
    args <- lapply(seq_len(ncol(tbl)), function(i) tbl[[i]])
    names(args) <- names(tbl)
    args <- c(list(".x" = .x), args)
    out <- do.call(add_params, args)
    check_post_add_params_settings(out)
    out
}

### ** Synonyms

#' @rdname add_params
#' @export
add_parameters <- add_params

#' @rdname add_params
#' @export
add_parameters_seq <- add_params_seq

#' @rdname add_params
#' @export
add_parameters_tbl <- add_params_tbl

### * add_settings*()

### ** add_settings()

#' Add settings to a simulation table
#'
#' @param .x A \code{sim_tbl} object. The only columns that it can have when
#'     calling an \code{add_settings*()} function are \code{"parameters"} and
#'     \code{"settings"}.
#' @param ... Settings to add, as named arguments. For \code{add_settings()},
#'     each argument is a vector containing all the values to add. For
#'     \code{add_settings_seq()}, each argument is a two-value vector specifying
#'     the min and the max values of the sequence to generate. See examples for
#'     more detail.
#' @param .tbl For \code{add_settings_tbl}, a data frame or tibble containing the
#'     settings to add.
#' @param .n For \code{add_settings_seq}, the number of values to generate when
#'     building a setting sequence.
#'
#' @return A \code{sim_tbl} object with the new settings added to the
#'     \code{"settings"} column.
#'
#' @family parameter/setting functions
#' 
#' @examples
#' z <- sim_tbl()
#'
#' z <- add_settings(z, alpha = rnorm(10), beta = rnorm(10))
#' settings(z)
#'
#' z <- add_settings_seq(z, gamma = c(-5, 5), .n = 10)
#' settings(z)
#'
#' tbl <- tibble::tibble(eta = runif(10), theta = rnorm(10))
#' z <- add_settings_tbl(z, tbl)
#' settings(z)
#'
#' z <- add_params(z, mu = rnorm(10)); z
#' params(z)
#' 
#' @export

add_settings <- function(.x, ...) {
    global_check_add_params_settings(.x = .x, ..., from = "add_settings")
    new_settings <- do.call(cbind, list(...))
    if ("settings" %in% colnames(.x)) {
        old_settings <- settings(.x)
        stopifnot(nrow(old_settings) == nrow(new_settings))
        new_settings <- cbind(old_settings, new_settings)
    }
    p_elements <- list()
    p_names <- colnames(new_settings)
    for (i in seq_len(nrow(new_settings))) {
        p_elements[[i]] <- unlist(new_settings[i, ])
        names(p_elements[[i]]) <- p_names
    }
    p_tbl <- tibble::tibble(settings = p_elements)
    if (nrow(.x) == 0) {
        .x <- p_tbl
    } else {
        .x[["settings"]] <- p_tbl[["settings"]]
    }
    check_post_add_params_settings(.x)
    as_sim_tbl(.x)
}

### ** add_settings_seq()

#' @rdname add_settings
#' @export

add_settings_seq <- function(.x, ..., .n = 2) {
    global_check_add_params_settings(.x = .x, ..., .from = "add_settings_seq")
    new_elements <- list(...)
    stopifnot(all(sapply(new_elements, length) == 2))
    new_elements <- lapply(new_elements, function(x) {
        seq(from = x[1], to = x[2], length.out = .n)
    })
    args <- c(list(".x" = .x), new_elements)
    out <- do.call(add_settings, args)
    check_post_add_params_settings(out)
    out
}

### ** add_settings_tbl()

#' @rdname add_settings
#' @export

add_settings_tbl <- function(.x, .tbl) {
    tbl <- check_add_params_settings_tbl(.tbl)
    args <- lapply(seq_len(ncol(tbl)), function(i) tbl[[i]])
    names(args) <- names(tbl)
    args <- c(list(".x" = .x), args)
    out <- do.call(add_settings, args)
    check_post_add_params_settings(out)
    out
}

### * cross_params*()

### ** cross_params()

#' Add parameters to a simulation table (crossing them with existing rows)
#'
#' \code{cross_params*()} and \code{cross_parameters*()} functions are synonymous.
#' 
#' @param .x A \code{sim_tbl} object. The only columns that it can have when
#'     calling a \code{cross_params*()} function are \code{"parameters"} and
#'     \code{"settings"}.
#' @param ... Parameters to add, as named arguments. For \code{cross_params()},
#'     each argument is a vector containing all the values to add. For
#'     \code{cross_params_seq()}, each argument is a two-value vector
#'     specifying the min and the max values of the sequence to generate. See
#'     examples for more detail. For \code{cross_params()} and
#'     \code{cross_params_seq()}, all new parameters are crossed between
#'     themselves, and with the existing rows of the simulation table.
#' @param tbl For \code{cross_params_tbl}, a data frame or tibble containing
#'     the parameters to add. All rows of the \code{tbl} are crossed with all
#'     existing rows of the simulation table, so the output has \code{nrow(.x)
#'     nrow(tbl)} rows.
#' @param .n For \code{cross_params_seq}, the number of values to generate when
#'     building a parameter sequence.
#'
#' @return A \code{sim_tbl} object with the new parameters crossed with the
#'     existing rows of the simulation table and added to the
#'     \code{"parameters"} column.
#'
#' @family parameter/setting functions
#' 
#' @examples
#' # cross_params
#' z <- sim_table() %>%
#'   add_params(alpha = rnorm(5), beta = rnorm(5)) %>%
#'   add_settings(n = 1:5)
#' z
#' z <- z %>%
#'   cross_params(eta = c(0.5, 1), theta = c(2, 4))
#' z
#' 
#' @export

cross_params <- function(.x, ...) {
    global_check_add_params_settings(.x = .x, ..., from = "cross_params")
    tbl1 <- params(.x, return_null = TRUE)
    tbl2 <- settings(.x, return_null = TRUE)
    new_tbls <- cross_tbl1_tbl2(.tbl1 = tbl1, .tbl2 = tbl2, ...)
    out <- new_tbls[[1]]
    colnames(out) <- "parameters"
    out[["settings"]] <- new_tbls[[2]][[1]]
    check_post_add_params_settings(out)
    as_sim_tbl(out)
}

### ** cross_params_seq()

#' @rdname cross_params
#' @export
#'
#' @examples
#' # cross_params_seq
#' z <- sim_table() %>%
#'   add_params(alpha = rnorm(3), beta = rnorm(3)) %>%
#'   add_settings(n = 1:3)
#' 
#' z <- z %>%
#'   cross_params_seq(eta = c(0.5, 1), theta = c(2, 4), .n = 5)
#' z

cross_params_seq <- function(.x, ..., .n = 2) {
    global_check_add_params_settings(.x = .x, ..., .from = "cross_params_seq")
    new_elements <- list(...)
    stopifnot(all(sapply(new_elements, length) == 2))
    new_elements <- lapply(new_elements, function(x) {
        seq(from = x[1], to = x[2], length.out = .n)
    })
    args <- c(list(".x" = .x), new_elements)
    out <- do.call(cross_params, args)
    check_post_add_params_settings(out)
    out
}

### ** cross_params_tbl()

#' @rdname cross_params
#' @export
#' 
#' @examples
#' # cross_params_tbl
#' z <- sim_table() %>%
#'   add_params(alpha = rnorm(3), beta = rnorm(3)) %>%
#'   add_settings(n = 1:3)
#'
#' tbl <- tibble::tibble(eta = 1:3, theta = 4:6)
#' z <- z %>%
#'   cross_params_tbl(tbl)
#' z

cross_params_tbl <- function(.x, .tbl) {
    tbl <- check_add_params_settings_tbl(.tbl)
    args <- lapply(seq_len(ncol(tbl)), function(i) tbl[[i]])
    names(args) <- colnames(tbl)
    args <- c(list(".x" = .x), args, list(from = "cross_params_tbl"))
    do.call(global_check_add_params_settings, args)
    tbl1 <- params(.x, return_null = TRUE)
    tbl2 <- settings(.x, return_null = TRUE)
    new_tbls <- cross_tbl1_tbl2_tbl1b(.tbl1 = tbl1, .tbl2 = tbl2, .tbl1b = tbl)
    out <- new_tbls[[1]]
    colnames(out) <- "parameters"
    out[["settings"]] <- new_tbls[[2]][[1]]
    check_post_add_params_settings(out)
    as_sim_tbl(out)
}

### ** Synonyms

#' @rdname cross_params
#' @export
cross_parameters <- cross_params

#' @rdname cross_params
#' @export
cross_parameters_seq <- cross_params_seq

#' @rdname cross_params
#' @export
cross_parameters_tbl <- cross_params_tbl

### * cross_settings*()

### ** cross_settings()

#' Add settings to a simulation table (crossing them with existing rows)
#'
#' @param .x A \code{sim_tbl} object. The only columns that it can have when
#'     calling a \code{cross_settings*()} function are \code{"parameters"} and
#'     \code{"settings"}.
#' @param ... Settings to add, as named arguments. For \code{cross_settings()},
#'     each argument is a vector containing all the values to add. For
#'     \code{cross_settings_seq()}, each argument is a two-value vector
#'     specifying the min and the max values of the sequence to generate. See
#'     examples for more detail. For \code{cross_settings()} and
#'     \code{cross_settings_seq()}, all new settings are crossed between
#'     themselves, and with the existing rows of the simulation table.
#' @param .tbl For \code{cross_settings_tbl}, a data frame or tibble containing
#'     the settings to add. All rows of the \code{tbl} are crossed with all
#'     existing rows of the simulation table, so the output has \code{nrow(.x)
#'     nrow(tbl)} rows.
#' @param .n For \code{cross_settings_seq}, the number of values to generate
#'     when building a setting sequence.
#'
#' @return A \code{sim_tbl} object with the new settings crossed with the
#'     existing rows of the simulation table and added to the
#'     \code{"settings"} column.
#'
#' @family parameter/setting functions
#' 
#' @examples
#' # cross_settings
#' z <- sim_table() %>%
#'   add_params(alpha = rnorm(5), beta = rnorm(5)) %>%
#'   add_settings(n = 1:5)
#' z
#' z <- z %>%
#'   cross_settings(eta = c(0.5, 1), theta = c(2, 4))
#' z
#' 
#' @export

cross_settings <- function(.x, ...) {
    global_check_add_params_settings(.x = .x, ..., from = "cross_settings")
    tbl1 <- settings(.x, return_null = TRUE)
    tbl2 <- params(.x, return_null = TRUE)
    new_tbls <- cross_tbl1_tbl2(.tbl1 = tbl1, .tbl2 = tbl2, ...)
    out <- new_tbls[[1]]
    colnames(out) <- "settings"
    out[["parameters"]] <- new_tbls[[2]][[1]]
    check_post_add_params_settings(out)
    as_sim_tbl(out)
}

### ** cross_settings_seq()

#' @rdname cross_settings
#' @export
#'
#' @examples
#' # cross_params_seq
#' z <- sim_table() %>%
#'   add_params(alpha = rnorm(3), beta = rnorm(3)) %>%
#'   add_settings(n = 1:3)
#' 
#' z <- z %>%
#'   cross_settings_seq(eta = c(0.5, 1), theta = c(2, 4), .n = 5)
#' z

cross_settings_seq <- function(.x, ..., .n = 2) {
    global_check_add_params_settings(.x = .x, ..., .from = "cross_settings_seq")
    new_elements <- list(...)
    stopifnot(all(sapply(new_elements, length) == 2))
    new_elements <- lapply(new_elements, function(x) {
        seq(from = x[1], to = x[2], length.out = .n)
    })
    args <- c(list(".x" = .x), new_elements)
    out <- do.call(cross_settings, args)
    check_post_add_params_settings(out)
    out
}

### ** cross_settings_tbl()

#' @rdname cross_settings
#' @export
#' 
#' @examples
#' # cross_settings_tbl
#' z <- sim_table() %>%
#'   add_params(alpha = rnorm(3), beta = rnorm(3)) %>%
#'   add_settings(n = 1:3)
#'
#' tbl <- tibble::tibble(eta = 1:3, theta = 4:6)
#' z <- z %>%
#'   cross_settings_tbl(tbl)
#' z
#'
#' z <- sim_table() %>%
#'   cross_settings_tbl(tbl)
#'
#' z <- sim_table() %>%
#'   add_params(alpha = 1:5) %>%
#'   cross_settings_tbl(tbl)

cross_settings_tbl <- function(.x, .tbl) {
    tbl <- check_add_params_settings_tbl(.tbl)
    args <- lapply(seq_len(ncol(tbl)), function(i) tbl[[i]])
    names(args) <- colnames(tbl)
    args <- c(list(".x" = .x), args, list(from = "cross_settings_tbl"))
    do.call(global_check_add_params_settings, args)
    tbl1 <- settings(.x, return_null = TRUE)
    tbl2 <- params(.x, return_null = TRUE)
    new_tbls <- cross_tbl1_tbl2_tbl1b(.tbl1 = tbl1, .tbl2 = tbl2, .tbl1b = tbl)
    out <- new_tbls[[1]]
    colnames(out) <- "settings"
    out[["parameters"]] <- new_tbls[[2]][[1]]
    check_post_add_params_settings(out)
    as_sim_tbl(out)
}
