---
title: "Adding parameters and settings"
date: "`r Sys.Date()`"
vignette: >
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
  %\VignetteIndexEntry{Tutorial: Adding parameters and settings}
output:
  rmarkdown::html_vignette
---

```{r include = FALSE}
library(here)
options(rmarkdown.html_vignette.check_title = FALSE)
knitr::opts_knit$set(verbose = TRUE)
knitr::opts_chunk$set(fig.align = "center")
svg_fig_out_width <- "65%"
options(scipen = 6, width = 85)
set.seed(4)

library(etalon)
```

```{r comment = "", results = "asis", echo = FALSE}
# Enable ANSI colors in the Rmd output
options(crayon.enabled = TRUE)
old_hooks <- fansi::set_knit_hooks(knitr::knit_hooks)
```

Creating a `"parameters"` and a `"settings"` column is the first step in building a simulation table. Parameters and settings are both used for dataset generation.

They have a very similar role in `etalon`, but there is a difference between the two: a statistical model will usually try to estimate the original values of the `"parameters"` elements, while the `"settings"` elements are only used when generating datasets and `etalon` does not expect the model to estimate them.

For example, a distribution mean `mu` and a standard deviation `sigma` are typically `"parameters"` that we would like to estimate with the statistical model, while the sample size `n` might be important when generating a variety of datasets but we are not interested in estimating `n` with the model.

## How to set and access parameters and settings

Parameters and settings can be added to a simulation table using the `add_params*()`, `cross_params*()` and `add_settings*()`, `cross_settings*()` families of functions. Importantly, parameters and settings cannot be added to a simulation table that already has some columns depending on them, such as a `"dataset"` column.

### `add_*` functions

`add_params()` and `add_settings()` will simply add their arguments to an existing simulation table:
```{r }
z <- sim_table() %>%
  add_params(alpha = rnorm(4), beta = rnorm(4))
params(z)
```

<div class="alert-primary" role="alert">

`params()` is a convenient function that unpacks the parameter values from a simulation table. `settings()` does the same for settings. If you want to unpack both, you can even use `parsets()`.

</div>

If the simulation table already has a `"parameters"` or a `"settings"` column, then the provided arguments must have the same length as the number of rows already present:
```{r }
# Here new parameters are compatible with existing ones
add_params(z, gamma = runif(4))
```

```r
# Here the length of new parameters do not match the number of existing rows
add_params(z, gamma = runif(10))
```

```
Error in add_params(z, gamma = runif(10)) : 
  nrow(old_params) == nrow(new_params) is not TRUE
```

You can add as many new parameters and settings as you like, as long as their length is compatible with the existing rows:
```{r }
z <- z %>%
  add_params(eta = runif(4)) %>%
  add_settings(n = 1:4, species = c(10, 20, 25, 34)) %>%
  add_params(theta = runif(4), kappa = rnorm(4))
params(z)
settings(z)
```

### `add_*_seq()` functions

`add_params_seq()` and `add_settings_seq()` can be used to generate sequences of values to add to an existing simulation table:
```{r }
z <- sim_table() %>%
  add_params_seq(alpha = c(-10, 10), beta = c(-2, 2), .n = 6)
params(z)
```

Each argument defines the minimum and maximum value for a parameter or a setting, and `.n` gives the number of evenly spaced points to generate.

Again, you can use these functions as many times as you need, provided that the new elements length is always compatible with the existing rows:
```{r }
z <- sim_table() %>%
  add_params(alpha = rnorm(6), beta = runif(6)) %>%
  add_settings_seq(angle = c(-45, 45), light = c(0, 100), .n = 6)
parsets(z)
```

### `add_*_tbl()` functions

Sometimes you just want to prepare your parameters and settings by some other means, and provide them in a ready-made table to `etalon`. You can use `add_params_tbl()` and `add_settings_tbl()` for that:
```{r }
my_params <- data.frame(alpha = runif(5),
                        beta = rnorm(5),
                        gamma = runif(5))
my_settings <- data.frame(N = sample(1:1000, 5),
                          n_groups = c(2, rep(12, 4)))
z <- sim_table() %>%
  add_params_tbl(my_params) %>%
  add_settings_tbl(my_settings)
parsets(z)
```

### `cross_*` functions

While the `add_*` functions add new parameters and settings along existing rows, the `cross_*` functions cross the new elements with the existing rows when adding them:
```{r }
z <- sim_table() %>%
  add_settings(n = 1:3) %>%
  cross_params(mu = c(4, 10, 20))
parsets(z)
```

If several arguments are given to `cross_params()` or `cross_settings()`, they will be crossed together before being crossed with the existing rows:
```{r }
z <- sim_table() %>%
  add_settings(species = c(10, 20)) %>%
  cross_params(mu = c(2, 4, 6), sigma = c(1, 3))
parsets(z)
```

### `cross_*_seq` functions

`cross_*_seq()` are similar to `add_*_seq()` in that they generate a sequence of values before crossing the new elements with the existing rows:
```{r }
z <- sim_table() %>%
  cross_params_seq(alpha = c(-5, 5), beta = c(-3, 3), .n = 10) %>%
  cross_params_seq(sigma = c(0.1, 3), .n = 5) %>%
  cross_settings(n = c(5, 10))
parsets(z)
```

`cross_params_seq()` and `cross_settings_seq()` can typically be used to generate a regular grid of parameters or settings values to be explored with simulations.

### `cross_*_tbl()` functions

`cross_params_tbl()` and `cross_settings_tbl()` accept a ready-made table and cross each row of this table with the existing rows. This can be useful when one does not want to cross all parameters/settings, but when to keep some blocks always in sync:
```{r }
distrib_params <- data.frame(mu = c(10, 20),
                             sigma = c(1, 5))
environment <- data.frame(n_species = c(10, 20, 30),
                          food = c("low", "medium", "high"))
z <- sim_table() %>%
  add_params_tbl(distrib_params) %>%
  cross_settings_tbl(environment)
parsets(z)
```

Note the difference if we hadn't used `cross_settings_tbl()` but `cross_settings()` instead:
```{r }
z <- sim_table() %>%
  add_params_tbl(distrib_params) %>%
  cross_settings(n_species = c(10, 20, 30),
                 food = c("low", "medium", "high"))
parsets(z)
```

## Example

### Sampling parameters from a simplex

Let's assume that we want to have three parameters `c("a", "b", "c")` which always sum up to 1. This is equivalent to sampling a random point on a [2-simplex](https://en.wikipedia.org/wiki/Simplex#The_standard_simplex).

For this we have to built our table of parameter values ourselves before passing it to `add_params_tbl()`. Fortunately, there is an helpful [post on StackExchange](https://cs.stackexchange.com/questions/3227/uniform-sampling-from-a-simplex) explaining how to sample from a simplex:
```{r }
sample_from_simplex <- function() {
  diff(sort(c(0, 1, runif(n = 2))))
}
sample_from_simplex()
```

Lets' build a table with our parameter values:
```{r }
n_sims <- 100
my_params <- replicate(n_sims, sample_from_simplex()) %>% t()
colnames(my_params) <- c("a", "b", "c")
head(my_params)
```

Now we can use this table with `add_params_tbl()`:
```{r }
z <- sim_table() %>%
  add_params_tbl(my_params)
params(z)
```

## Next

Now it is time to generate datasets from the parameters and settings values you created: [see the next vignette](tutorial-030-generate-data.html).
