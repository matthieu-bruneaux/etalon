data {
  int<lower=1> n; // Number of observations
  int<lower=1> N; // Number of distinct birds
  int<lower=1, upper=N> bird_ids[n]; // Array of int containing the bird ids
  int<lower=1, upper=2> prey_color[n]; // Array of prey colors (1=black, 2=red)
  int<lower=0, upper=1> attack[n]; // Response variable
}

parameters {
  real<lower=0, upper=1> p_black;
  real<lower=0, upper=1> p_red;
}

transformed parameters {
  real<lower=0, upper=1> p_prey[2]; // [p_black, p_red]
  real logit_p_prey[2];
  p_prey[1] = p_black;
  p_prey[2] = p_red;
  for (i in 1:2) {
    logit_p_prey[i] = log(p_prey[i] / (1 - p_prey[i]));
  }
}

model {
  for (i in 1:n) {
    attack[i] ~ bernoulli_logit(logit_p_prey[prey_color[i]]);
  }
  p_black ~ uniform(0, 1);
  p_red ~ uniform(0, 1);
}
