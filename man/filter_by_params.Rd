% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/ui_main-others.R
\name{filter_by_params}
\alias{filter_by_params}
\title{Filter rows of a simulation table based on the parameters values}
\usage{
filter_by_params(x, ...)
}
\arguments{
\item{x}{A \code{\link[=sim_tbl]{simulation table}}.}

\item{...}{Passed to \code{\link[dplyr]{filter}} to filter \code{params(x)}.}
}
\value{
The input x table, filtered based on its parameters values.
}
\description{
Filter rows of a simulation table based on the parameters values
}
\examples{
x <- sim_tbl() \%>\%
  cross_parameters_seq(alpha = c(-5, 5), beta = c(-2, 2), .n = 5) \%>\%
  cross_settings_seq(n = c(5, 50), .n = 3) \%>\%
  replicate_sims(10)
filter_by_params(x, alpha > 0 & beta > 1)

}
\seealso{
\code{\link{filter_by_settings}}
}
