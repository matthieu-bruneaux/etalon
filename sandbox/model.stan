// Model is y = alpha + beta1 * x1 + beta2 * x2 + normal(0, sigma)

/// * Data

data {
  int<lower=1> n; // Sample size
  real y[n]; // Observations
  real x1[n]; // Predictor 1
  real x2[n]; // Predictor 2
}

/// * Parameters

parameters {
  real alpha;
  real beta1;
  real beta2;
  real<lower=0> sigma;
}

/// * Transformed parameters

transformed parameters {
  real y_pred[n];
  for (i in 1:n) {
    y_pred[i] = alpha + beta1 * x1[i] + beta2 * x2[i];
  }
}

/// * Model

model {

  /// * Priors
  alpha ~ normal(0, 10);
  beta1 ~ normal(0, 10);
  beta2 ~ normal(0, 10);
  sigma ~ normal(0, 10);

  /// * Likelihood
  for (i in 1:n) {
    y[i] ~ normal(y_pred[i], sigma);
  }
  
}
