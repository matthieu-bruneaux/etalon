### * Setup

devtools::load_all()

### * Looking into lme4::sleepstudy data

library(R2jags)
library(lme4)

jags_data <- list(n = nrow(sleepstudy),
                  reaction = sleepstudy$Reaction,
                  days = sleepstudy$Days,
                  ind = as.numeric(as.factor(sleepstudy$Subject)))
jags_data[["n_ind"]] <- max(jags_data[["ind"]])

jags_lm <- function() {
    # Priors
    alpha ~ dunif(-500, 500)
    beta ~ dunif(-500, 500)
    sigma ~ dunif(0, 1000)
    tau <- 1/sigma^2
    # Likelihood
    for (i in 1:n) {
        y_pred[i] <- alpha + beta * days[i]
        reaction[i] ~ dnorm(y_pred[i], tau)
    }
}

jags_mm <- function() {
    # Priors
    alpha ~ dunif(-500, 500)
    beta ~ dunif(-500, 500)
    sigma ~ dunif(0, 1000)
    sigma_alpha ~ dunif(0, 1000)
    sigma_beta ~ dunif(0, 1000)
    tau <- 1/sigma^2
    tau_alpha <- 1/sigma_alpha^2
    tau_beta <- 1/sigma_beta^2
    # Likelihood
    for (i in 1:n_ind) {
        alpha_ind[i] ~ dnorm(0, tau_alpha)
        beta_ind[i] ~ dnorm(0, tau_beta)
    }
    for (i in 1:n) {
        y_pred[i] <- alpha + alpha_ind[ind[i]] + (beta + beta_ind[ind[i]]) * days[i]
        reaction[i] ~ dnorm(y_pred[i], tau)
    }
}

jags_params_lm <- c("alpha", "beta", "sigma")
jags_params_mm <- c("alpha", "beta", "sigma", "sigma_alpha", "sigma_beta",
                    "alpha_ind", "beta_ind")

fit_lm <- jags(data = jags_data, model.file = jags_lm,
            parameters.to.save = jags_params_lm,
            n.chains = 4, n.iter = 5000)
isotracer::traceplot(as.mcmc(fit_lm)[, c("alpha", "beta", "sigma")])

fit_mm <- jags(data = jags_data, model.file = jags_mm,
            parameters.to.save = jags_params_mm,
            n.chains = 4, n.iter = 20000)
isotracer::traceplot(as.mcmc(fit_mm))
isotracer::traceplot(as.mcmc(fit_mm)[, c("alpha", "beta", "sigma")])

summary(as.mcmc(fit_lm)[, c("alpha", "beta", "sigma")])
summary(as.mcmc(fit_mm)[, c("alpha", "beta", "sigma")])

### * Formatting sim table

library(pillar)
library(sloop)

z <- sim_table() %>%
    set_params(mu = 10, var = c(0.5, 1, 2)) %>%
    set_settings(n = c(5, 10, 20)) %>%
    replicate_sims(100)  %>%
    generate_dataset(function(mu) rnorm(10))

s3_dispatch(print(z))
getAnywhere(print.tbl) # Find a method (because of the dot in the name)
getAnywhere(print_tbl) # Find a regular function
s3_dispatch(format(z))
getAnywhere(format.tbl)
getAnywhere(format_tbl)
# Now we found the core function doing the tbl formatting

s3_methods_generic("tbl_format_body")
getAnywhere(tbl_format_body.tbl)

s3_methods_generic("tbl_format_setup")
getAnywhere(tbl_format_setup.tbl)

w <- pillar::tbl_format_setup(z)

tbl_format_setup.sim_tbl <- function(x, width, ..., n, max_extra_cols, max_footer_lines) {
    w <- NextMethod()
    header <- w$body[1]
    header <- gsub("parameters", as.character(cli::col_green("parameters")), header)
    w$body[1] <- header
    w
}

library(crayon)

header_colors <- c("parameters" = "chartreuse2",
                   "dataset" = "darkorchid1",
                   "fit" = "deepskyblue",
                   "estimates" = "firebrick2")

tbl_format_setup.sim_tbl <- function(x, width, ..., n, max_extra_cols, max_footer_lines) {
    w <- NextMethod()
    header <- w$body[1]
    coloring <- c("parameters" = "chartreuse3",
                  "settings" = "chartreuse3",
                  "dataset" = "darkorchid1",
                  "derived" = "darkorchid1",
                  " fit" =  "deepskyblue",
                  "ppfit" =  "deepskyblue",
                  "estimates" = "firebrick2",
                  "intervals" = "firebrick2")
    for (i in names(coloring)) {
        header <- gsub(i, crayon::make_style(coloring[i])(i), header, fixed = TRUE)
    }
    w$body[1] <- header
    w
}

# Colors in R markdown
# https://cran.r-project.org/web/packages/fansi/vignettes/sgr-in-rmd.html
# https://stackoverflow.com/questions/47392839/crayon-in-r-markdown-knitr-reports
