[![GitLab pipeline status](https://gitlab.com/matthieu-bruneaux/etalon/badges/master/pipeline.svg)](https://gitlab.com/matthieu-bruneaux/etalon/commits/master)
[![Coverage report](https://gitlab.com/matthieu-bruneaux/etalon/badges/master/coverage.svg)](https://matthieu-bruneaux.gitlab.io/etalon/coverage/coverage.html)
[![R_CMD_CHECK](https://matthieu-bruneaux.gitlab.io/etalon/R-CMD-check_badge.svg)](https://matthieu-bruneaux.gitlab.io/etalon/R-CMD-check_output.txt)
[![Lifecycle Status](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://www.tidyverse.org/lifecycle/)

etalon: an R package to evaluate statistical models <img src="man/figures/hexsticker_blueprints.png" width="120" align="right" />
===================================================

`etalon` provides a simple toolkit to evaluate the performance of statistical models.

In a nutshell, it helps to:

- **generate a grid** of known values for parameters
- **simulate many datasets** based on those parameter values
- **run the chosen statistical model** on the simulated datasets
- **compare** the model results with the known parameter values.

It can be used profitably when preparing an experiment to benchmark experimental designs and statistical models. 

It can also be used when developing new statistical models to check their ability to capture the true parameter values.

## Installation and quick start

You can install the `etalon` package from R with:

```
install.packages("devtools")
devtools::install_gitlab("matthieu-bruneaux/etalon", quiet = TRUE)
```

Have a [look at the Quick start vignette](articles/tutorial-010-quick-start.html) to get started!

*The package is not on CRAN at the moment, but hopefully it will be submitted once a stable first version is achieved.*

## Documentation

Have a look at the [vignettes](https://matthieu-bruneaux.gitlab.io/etalon/articles/index.html) to learn how to use the package!
